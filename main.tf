provider "aws" {
  region = "us-east-1"
}
variable "key_name" {
  default = "somekp"
}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

data "aws_region" "region" {}

locals {
  region  = data.aws_region.region.name
  segment = "test1"
}


resource "local_file" "kp" {
  content  = tls_private_key.example.private_key_pem
  filename = "${path.module}/${var.key_name}.pem"
}


resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.example.public_key_openssh
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*amzn2-ami-hvm-2*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # Canonical
}

data "aws_subnet_ids" "example" {
  vpc_id = data.aws_vpc.vpc.id
}

output "subnets" {
  value = data.aws_subnet_ids.example.ids
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon_linux_2.id
  instance_type = "t2.micro"
  key_name      = "somekp"
  tags = {
    Name = "HelloWorld"
  }
  subnet_id = element(tolist(data.aws_subnet_ids.example.ids),length(data.aws_subnet_ids.example.ids)-1)
  vpc_security_group_ids  = [aws_security_group.allow_tls.id]
  user_data_base64 = filebase64("./bootstrap.sh")
}


data "aws_vpc" "vpc" {
  tags = {
    segment = local.segment
    region  = local.region
  }
}

resource "aws_security_group" "allow_tls" {
  name   = "ssh-http-sg"
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    Name = "ssh-http-sg"
  }
}

resource "aws_security_group_rule" "ingress_22" {
  description       = "TLS from VPC"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_tls.id
  type              = "ingress"

}

resource "aws_security_group_rule" "ingress_80" {
  description       = "TLS from VPC"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_tls.id
  type              = "ingress"
}

resource "aws_security_group_rule" "egress_443" {
  description       = "TLS from VPC"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_tls.id
  type              = "egress"
}

output "public_ip" {
  value = aws_instance.web.public_ip
}

output "ami" {
  value = data.aws_ami.amazon_linux_2.id
}